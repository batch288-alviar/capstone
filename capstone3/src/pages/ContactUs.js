import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ContactUs() {
  return (
    <Container fluid className="mt-5">
      <Row>
        <Col className="col-8 offset-2 my-5">
          <h1>Contact Us</h1>
          <p>Feel free to get in touch with us for any inquiries or feedback.</p>
          <Form>
            <Row>
              <Col>
                <Form.Group className="mb-3" controlId="formFirstName">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter First Name" required />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group className="mb-3" controlId="formLastName">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter Last Name" required />
                </Form.Group>
              </Col>
            </Row>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" required />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicMessage">
              <Form.Label>Message</Form.Label>
              <Form.Control as="textarea" rows={5} placeholder="Enter your message" required />
            </Form.Group>
            <Button
              className="mt-3"
              variant="dark"
              style={{
                backgroundColor: 'black',
                color: 'white',
                borderRadius: '0',
                width: '100%',
                padding: '10px 0',
                fontSize: '1rem',
              }}
              type="submit"
            >
              Submit
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
