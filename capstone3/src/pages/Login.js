import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function Login() {
  const [isDisabled, setIsDisabled] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);

  const login = (event) => {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data === false) {
          Swal2.fire({
            title: 'Authentication failed!',
            icon: 'error',
            text: 'Check your login details and try again!',
            showConfirmButton: false,
          });
        } else {
          localStorage.setItem('token', data.auth);

          retrieveUserDetails(data.auth);

          Swal2.fire({
            title: 'Login Successful',
            icon: 'success',
            text: 'Welcome to Watch Couture!',
            showConfirmButton: false,
          });

          navigate('/');
        }
      });
  };

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/user/userData`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email && password) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password]);

  useEffect(() => {
    if (user !== null) {
      navigate('/');
    }
  }, [user]);

  return (
    <Container className="mt-5">
      <Row>
        <Col className="col-8 offset-2 my-5">
          <h1> LOG IN</h1>
          <p> Your personal account enables you to check your watches in and benefit from additional exclusive features.</p>
          <Form onSubmit={login}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" value={email} onChange={(event) => setEmail(event.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" value={password} onChange={(event) => setPassword(event.target.value)} />
            </Form.Group>

            <Button
              className="mt-3"
              variant="dark"
              style={{ backgroundColor: 'black', color: 'white', borderRadius: '0', width: '100%', padding: '10px 0', fontSize: '1rem' }}
              type="submit"
              disabled={isDisabled}
            >
              Login
            </Button>

            <p className="mt-3 text-center">No account yet? Sign up here.</p>

            <Link to="/register">
              <Button className="mt-0" variant="light" style={{ backgroundColor: 'white', color: 'black', borderRadius: '0', width: '100%', padding: '10px 0', fontSize: '1rem' }}>
                Create Account
              </Button>
            </Link>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
