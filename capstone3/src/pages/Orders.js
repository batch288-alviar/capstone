import React, { useEffect, useState, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import OrderCard from '../components/OrderCard.js';
import { Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext.js';

export default function Orders() {
  const [orders, setOrders] = useState([]);
  const [userLoaded, setUserLoaded] = useState(false);
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!user) {
      navigate('/');
    } else {
      fetchUserOrders();
    }
  }, [user, navigate]);

  useEffect(() => {
    setUserLoaded(true);
  }, [user]);

  const fetchUserOrders = () => {
    fetch(`${process.env.REACT_APP_API_URL}/user/${user.id}/orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
    })
      .then((result) => result.json())
      .then((data) => {
        if (Array.isArray(data)) {
          setOrders(data);
        } else {
          setOrders([]);
        }
      })
      .catch((error) => {
        console.log('Error fetching user orders:', error);
      });
  };

  if (!userLoaded) {
    return <p>Loading user...</p>;
  }

  if (orders.length === 0) {
    return (
      <Container style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <h1>No Orders Made Yet!</h1>
      </Container>
    );
  }

  return (
    <>
      <Container>
        <Row className="products-row-custom">
          <Col>
            <h1 className="text-center mt-3">Orders</h1>
              {orders.map((order) => (
                <OrderCard key={order._id} order={order} />
              ))}
          </Col>
        </Row>
      </Container>
    </>
  );
}
