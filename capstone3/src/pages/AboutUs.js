import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

export default function AboutUs() {
  return (
    <Container className="about-container">
      <Row>
        <Col lg={6} className="d-flex align-items-center">
          <div className="about-text">
            <h1>About Us</h1>
            <p>
              Welcome to our luxury watch shop! With over 20 years of experience,
              we have established ourselves as a leading provider of the most
              exquisite and prestigious timepieces.
            </p>
            <p>
              Our passion for watches and commitment to craftsmanship has made us
              a trusted destination for watch enthusiasts worldwide. We curate a
              carefully selected collection of the finest luxury watches,
              featuring renowned brands known for their exceptional quality and
              timeless elegance.
            </p>
            <p>
              At our shop, we believe that a watch is more than just a timekeeping
              device; it's an expression of personal style and a symbol of
              sophistication. Whether you're seeking a classic, sporty, or
              avant-garde design, our dedicated team of watch experts is here to
              assist you in finding the perfect timepiece that reflects your
              individuality.
            </p>
            <p>
              We invite you to visit our store and immerse yourself in the world
              of luxury watches. Experience the unparalleled beauty, craftsmanship,
              and precision that our timepieces offer. We look forward to serving
              you and helping you discover the watch of your dreams.
            </p>
          </div>
        </Col>
        <Col lg={6} className="d-none d-lg-flex align-items-center justify-content-center">
          <div className="about-image">
            <img
              src="https://www.awco.nl/wp-content/uploads/2023/01/awco-pui-2022-jan-3-e1673610251576-768x961.jpg"
              alt="Luxury Watches"
              className="img-fluid"
            />
          </div>
        </Col>
      </Row>
    </Container>
  );
}
