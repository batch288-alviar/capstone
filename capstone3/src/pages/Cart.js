import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { Container, Row, Button } from 'react-bootstrap';
import Swal2 from 'sweetalert2';
import CartCard from '../components/CartCard';
import UserContext from '../UserContext.js';
 
export default function Cart() {
  const [products, setProducts] = useState([]);
  const { user } = useContext(UserContext);
  const [total, setTotal] = useState(0);
  const [selectedProductIds, setSelectedProductIds] = useState([]);
  const [isDeleting, setIsDeleting] = useState(false);
  const [isFetchingCart, setIsFetchingCart] = useState(true);
  const [isOrderDisabled, setIsOrderDisabled] = useState(true); // New state for order button
  const Navigate = useNavigate();

  useEffect(() => {
    if (user === null) {
      Navigate('/');
    } else {
      fetch(`${process.env.REACT_APP_API_URL}/user/${user.id}/viewCart`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json',
        },
      })
        .then((result) => result.json())
        .then((data) => {
          setProducts(data.products);
          setIsFetchingCart(false);
        })
        .catch((error) => {
          console.log('Error fetching products:', error);
          setIsFetchingCart(false);
        });
    }
  }, [user, isDeleting]);

  useEffect(() => {
    setIsOrderDisabled(selectedProductIds.length === 0);
  }, [selectedProductIds]);

  const handleCheckboxChange = (productId, isChecked, subTotal) => {
    if (isChecked) {
      setSelectedProductIds((prevSelectedProductIds) => [...prevSelectedProductIds, productId]);
      setTotal((prevTotal) => prevTotal + subTotal);
    } else {
      setSelectedProductIds((prevSelectedProductIds) =>
        prevSelectedProductIds.filter((id) => id !== productId)
      );
      setTotal((prevTotal) => prevTotal - subTotal);
    }
  };

  const fetchCartProducts = () => {
    setIsDeleting(true);
    fetch(`${process.env.REACT_APP_API_URL}/user/${user.id}/viewCart`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
    })
      .then((result) => result.json())
      .then((data) => {
        setProducts(data.products);
        setIsDeleting(false);
      })
      .catch((error) => {
        console.log('Error fetching products:', error);
        setIsDeleting(false);
      });
  };

  const handleDeleteProduct = () => {
    setIsDeleting(true);
  };

  const handleOrderButtonClick = () => {
    if (selectedProductIds.length === 0) {
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/user/${user.id}/createOrder`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id: selectedProductIds }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log('Order created:', data);
        fetchCartProducts();
        setTotal(0);
        Swal2.fire({
          title: 'Order Created!',
          icon: 'success',
          text: 'Thank you for choosing our services!',
          showConfirmButton: false,
        });
        Navigate('/orders')
      })
      .catch((error) => {
        console.log('Error creating order:', error);
      });
  };

  if (isFetchingCart) {
    return <div>Loading...</div>;
  }

  if (user.isAdmin) {
    return <Navigate to="/" />;
  }

  if (products.length === 0) {
    return (
      <Container style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <h1>No products in your cart yet!</h1>
      </Container>
    );
  }

  return (
    <Container>
      <Row>
        <h1>Cart</h1>
      </Row>
      <Row>
        {products.map((product) => (
          <CartCard
            key={product.productId}
            productProp={product}
            handleCheckboxChange={handleCheckboxChange}
            onDeleteProduct={fetchCartProducts}
          />
        ))}
      </Row>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <p className="mt-2">Total Amount of Selected:</p>
        <p className="mt-2" style={{ fontWeight: 'bold', marginLeft: 'auto' }}>
          {total.toLocaleString()}
        </p>
      </div>
      <Button variant="dark" className="w-100" onClick={handleOrderButtonClick} disabled={isOrderDisabled}>
        Order
      </Button>
    </Container>
  );
}
