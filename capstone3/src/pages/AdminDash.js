import React, { useEffect, useState, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import AdminProductCard from '../components/AdminProductCard.js';
import { Container, Row, Col, Card, Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function AdminDash() {
  const [products, setProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [productData, setProductData] = useState({
    brand: '',
    name: '',
    price: '',
    description: '',
  });

  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((result) => result.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <AdminProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  const fetchProducts = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((result) => result.json())
      .then((data) => {
        setProducts(
          data.map((product) => (
            <AdminProductCard key={product._id} productProp={product} />
          ))
        );
      });
  };

  useEffect(() => {
    if (user !== null) {
      setIsLoading(false);
    }
  }, [user]);

  const handleModalOpen = () => {
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
    setProductData({
      brand: '',
      name: '',
      price: '',
      description: '',
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(productData),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          handleModalClose();
          fetchProducts();
          Swal2.fire({
            title: 'Product has been Added!',
            icon: 'success',
            text: 'Product has successfully been Added!',
            showConfirmButton: false,
          });
        } else {
          Swal2.fire({
            title: 'Product was not Added!',
            icon: 'error',
            text: `${productData.name} already exists!`,
            showConfirmButton: false,
          });
        }
      });
  };

  const navigate = useNavigate();

  useEffect(() => {
    if (user === null) {
      navigate('/');
    }
  }, [user, navigate]);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <h1 className="text-center mt-3">Admin Dashboard</h1>

      <div className="text-center">
        <Button onClick={handleModalOpen} variant="dark">
          Create new product
        </Button>
        <Link to="/allOrders" className="btn btn-dark ms-3">
          Get all orders
        </Link>
      </div>

      <Container>
        <Row>
          <Col>
            <Modal show={showModal} onHide={handleModalClose}>
              <Modal.Header closeButton>
                <Modal.Title>Create New Product</Modal.Title>
              </Modal.Header>

              <Modal.Body>
                <Form.Group controlId="brand">
                  <Form.Label>Brand</Form.Label>
                  <Form.Control
                    type="text"
                    value={productData.brand}
                    onChange={(event) =>
                      setProductData({
                        ...productData,
                        brand: event.target.value,
                      })
                    }
                    required
                  />
                </Form.Group>
                <Form onSubmit={handleSubmit}>
                  <Form.Group controlId="name">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      type="text"
                      value={productData.name}
                      onChange={(event) =>
                        setProductData({
                          ...productData,
                          name: event.target.value,
                        })
                      }
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="number"
                      value={productData.price}
                      onChange={(event) =>
                        setProductData({
                          ...productData,
                          price: event.target.value,
                        })
                      }
                      required
                    />
                  </Form.Group>
                  <Form.Group controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={3}
                      value={productData.description}
                      onChange={(event) =>
                        setProductData({
                          ...productData,
                          description: event.target.value,
                        })
                      }
                      required
                    />
                  </Form.Group>
                  <Button variant="dark" type="submit" className="mt-5">
                    Create
                  </Button>
                </Form>
              </Modal.Body>
            </Modal>
          </Col>
        </Row>
      </Container>
      {products}
    </>
  );
}
