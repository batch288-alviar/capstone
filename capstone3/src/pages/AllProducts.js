import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import GlobalProductCard from '../components/GlobalProductCard.js';
import { Container, Row, Col } from 'react-bootstrap';

export default function Products() {
  const [products, setProducts] = useState([]);
  const { productId } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((result) => result.json())
      .then((data) => {
        const activeProducts = data.filter((product) => product.isActive);
        setProducts(activeProducts);
      });
  }, []);

  return (
    <>
      <Container>
        <Row className="products-row-custom">
          <Col>
            <h1 className="text-center mt-3">Products</h1>
            <Row>
              {products.map((product) => (
                <GlobalProductCard key={product._id} productProp={product} />
              ))}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
}
