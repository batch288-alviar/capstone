import React, { useEffect, useState, useContext } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import AdminOrderCard from '../components/AdminOrderCard.js';
import { Container, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext.js';

export default function Orders() {
  const [orders, setOrders] = useState([]);
  const [userLoaded, setUserLoaded] = useState(false);
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  useEffect(() => {
    if (user) {
      fetchAllUserOrders();
    }
  }, [user]);

  useEffect(() => {
    setUserLoaded(true);
  }, [user]);

  const fetchAllUserOrders = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
    })
      .then((result) => result.json())
      .then((data) => {
        if (Array.isArray(data)) {
          setOrders(data);
        } else {
          setOrders([]);
        }
      })
      .catch((error) => {
        console.log('Error fetching user orders:', error);
      });
  };

  if (!userLoaded) {
    return <p>Loading user...</p>;
  }

  if (user === null || user.isAdmin === false) {
    navigate('/');
    return null; // Render nothing while redirecting
  }

  return (
    <>
      <Container>
        <Row className="products-row-custom">
          <Col>
            <h1 className="text-center mt-3">Orders</h1>
              {orders.map((order) => (
                <AdminOrderCard key={order._id} order={order} />
              ))}
          </Col>
        </Row>
      </Container>
    </>
  );
}
