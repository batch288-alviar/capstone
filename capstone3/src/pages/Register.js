import React, { useState, useEffect, useContext } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import UserContext from '../UserContext.js';
import Swal2 from 'sweetalert2';

export default function Register() {
  const [salutation, setSalutation] = useState('');
  const [firstName, setFirst] = useState('');
  const [lastName, setLast] = useState('');
  const [address, setAddress] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [isDisabled, setIsDisabled] = useState(true);

  useEffect(() => {
    if (
      salutation !== '' &&
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      address !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2 &&
      password1.length > 6
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [salutation, firstName, lastName, address, email, password1, password2]);

  useEffect(() => {
    if (user !== null) {
      navigate('/');
    }
  }, [user]);

  function register(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        address: address,
        password: password1,
        email: email,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        if (data === false) {
          Swal2.fire({
            title: 'Duplicate user found',
            icon: 'error',
            text: 'Please use a different email or try logging in.',
            showConfirmButton: false,
          });
        } else {
          Swal2.fire({
            title: 'Success',
            icon: 'success',
            text: 'You are now registered!',
            showConfirmButton: false,
          });
          navigate('/login');
        }
      });
  }

  return (
    <Container fluid className="mt-5">
      <Row>
        <Col className="col-8 offset-2 my-5">
          <h1>CREATE AN ACCOUNT</h1>
          <p>Your personal account enables you to check your watches in and benefit from additional exclusive features.</p>
          <Form onSubmit={register}>
            <Row>
              <Col>
                <Form.Group className="mb-3" controlId="salutation">
                  <Form.Label>Salutation</Form.Label>
                  <Form.Select value={salutation} onChange={(event) => setSalutation(event.target.value)}>
                    <option value="">Select</option>
                    <option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                    <option value="Ms">Ms</option>
                  </Form.Select>
                </Form.Group>
              </Col>
              <Col>
                <Form.Group className="mb-3" controlId="formFirstName">
                  <Form.Label>First Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={(event) => setFirst(event.target.value)} />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group className="mb-3" controlId="formLastName">
                  <Form.Label>Last Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={(event) => setLast(event.target.value)} />
                </Form.Group>
              </Col>
            </Row>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" value={email} onChange={(event) => setEmail(event.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicAddress">
              <Form.Label>Address</Form.Label>
              <Form.Control type="text" placeholder="Enter address" value={address} onChange={(event) => setAddress(event.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" value={password1} onChange={(event) => setPassword1(event.target.value)} />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicPassword2">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control type="password" placeholder="Retype your password" value={password2} onChange={(event) => setPassword2(event.target.value)} />
            </Form.Group>
            <p>
              Have an account already? <Link to="/login">Log in here</Link>.
            </p>
            <Button
              className="mt-3"
              variant="dark"
              style={{
                backgroundColor: 'black',
                color: 'white',
                borderRadius: '0',
                width: '100%',
                padding: '10px 0',
                fontSize: '1rem',
              }}
              type="submit"
              disabled={isDisabled}
            >
              Create an Account
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
