import { Container, Row, Col, Card } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function OrderCard(props) {
  const { _id, products, userId, totalAmount, purchasedOn } = props.order;
  const productIds = products.map((product) => product.productId);
  const [productData, setProductData] = useState([]);

  useEffect(() => {
    if (products && products.length > 0) {
      fetchProducts();
    }
  }, [products]);

  const fetchProducts = () => {
    const productIds = products.map((product) => product.productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/orderProducts`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ productIds }),
    })
      .then((response) => response.json())
      .then((data) => {
        setProductData(data);
      })
      .catch((error) => {
        console.error('Error retrieving products:', error);
      });
  };

  const productBrands = productData.map((product) => product.brand);
  const productNames = productData.map((product) => product.name);


  return (
    <Container>
      <Row>
        <Col className="mt-3">
          <Card className="border border-black">
            <Card.Body>
              <Card.Title>Order #{_id}</Card.Title>

              {productData.map((product) => (
                <div key={product._id} className="mb-2 ms-2">
                  <Card.Text>{product.brand} {product.name}</Card.Text>
                </div>
              ))}

              <Card.Text className="mb-2">Total Amount: PhP {totalAmount.toLocaleString()}</Card.Text>
              <Card.Text>Purchased On: {purchasedOn}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
