import { Container, Row, Col, Card } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function AdminOrderCard(props) {
  const { _id, products, userId, totalAmount, purchasedOn } = props.order;
  const [userData, setUserData] = useState(null);
  const productIds = products.map((product) => product.productId);
  const [productData, setProductData] = useState([]);

  useEffect(() => {
    fetchUserData(userId);
  }, [userId]);

  const fetchUserData = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/user/userDataOrder`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        userId: userId,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        setUserData(data);
      })
      .catch((error) => {
        console.error('Error retrieving user data:', error);
        Swal2.fire({
          title: 'Error',
          icon: 'error',
          text: 'Failed to retrieve user data',
          showConfirmButton: false,
        });
      });
  };

  useEffect(() => {
    if (products && products.length > 0) {
      fetchProducts();
    }
  }, [products]);

  const fetchProducts = () => {
    const productIds = products.map((product) => product.productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/orderProducts`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ productIds }),
    })
      .then((response) => response.json())
      .then((data) => {
        setProductData(data);
      })
      .catch((error) => {
        console.error('Error retrieving products:', error);
      });
  };

  const productBrands = productData.map((product) => product.brand);
  const productNames = productData.map((product) => product.name);




  return (
    <Container>
      <Row>
        <Col className="mt-3">
          <Card className="border border-black">
            <Card.Body>
              <Card.Title>{userData ? userData.firstName + ' ' + userData.lastName : 'Loading...'}</Card.Title>
              <Card.Text>Order Id: {_id}</Card.Text>
              {productData.map((product) => (
                <div key={product._id} className="mb-3 ms-2">
                  <Card.Text>{product.brand} {product.name}</Card.Text>
                </div>
              ))}
              <Card.Text>Total Amount: PhP {totalAmount.toLocaleString()}</Card.Text>
              <Card.Text>Purchased On: {purchasedOn}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
