import { Container, Row, Col, Card, Button, Modal, Form } from 'react-bootstrap';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import Swal2 from 'sweetalert2';

export default function BrandProductCard(props) {
  const { _id } = props.productProp;

  const [brand, setBrand] = useState(props.productProp.brand);
  const [name, setName] = useState(props.productProp.name);
  const [description, setDescription] = useState(props.productProp.description);
  const [price, setPrice] = useState(props.productProp.price);

  return (
    <Container>
      <Row>
        <Col className="mt-3">
          <Link to={`/products/${_id}`} className="text-decoration-none">
            <Card className="border border-black">
              <Card.Body>
                <Card.Title>{brand} {name}</Card.Title>
                <Card.Subtitle className="mb-1">Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Text>Price: PhP {price.toLocaleString()}</Card.Text>
              </Card.Body>
            </Card>
          </Link>
        </Col>
      </Row>
    </Container>
  );
}
