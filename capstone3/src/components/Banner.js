import React from 'react';
import { Button, Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {
  return (
    <Container className="banner-container">
      <Row>
        <Col>
          <Link to="/products" className="banner-link">
            <h1 className="homeHeader-custom">DISCOVER</h1>
          </Link>
          <h2 className="homeSpan-custom">TIMELESS ELEGANCE</h2>
        </Col>
      </Row>
    </Container>
  );
}
