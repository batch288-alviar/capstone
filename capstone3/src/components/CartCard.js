import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Swal2 from 'sweetalert2'

export default function CartCard({ productProp, handleCheckboxChange, onDeleteProduct }) {
  const { productId, subTotal } = productProp;
  const [productName, setProductName] = useState('');
  const [isChecked, setIsChecked] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then((data) => {
        setProductName(data.name);
        setIsLoading(false);
      })
      .catch((error) => {
        console.log('Error fetching product:', error);
        setIsLoading(false);
      });
  }, [productId]);

  const handleCheckboxToggle = () => {
    setIsChecked(!isChecked);
    handleCheckboxChange(productId, !isChecked, subTotal);
  };

  const handleDeleteClick = () => {
    setIsDeleting(true);
    fetch(`${process.env.REACT_APP_API_URL}/user/${productId}/deleteCart`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.ok) {
          Swal2.fire({
            title: 'Product removed!',
            icon: 'success',
            text: 'Product has been removed from your cart!',
            showConfirmButton: false,
          })
          setIsLoading(true);
          onDeleteProduct();
        } else {
          console.log('Error deleting product');
        }
      })
      .catch((error) => {
        console.log('Error deleting product:', error);
      })
      .finally(() => {
        setIsDeleting(false);
      });
  };

  return (
    <Container>
      <Row>
        <Col className="mt-3">
          <Card className="border border-black">
            <Card.Body style={{ display: 'flex', alignItems: 'center' }}>
              <div style={{ marginRight: 'auto' }}>
                <Card.Title>{productName}</Card.Title>
                <Card.Text>Product ID: {productId}</Card.Text>
                <Card.Text>Subtotal: PhP {subTotal.toLocaleString()}</Card.Text>
              </div>
              <div style={{ marginLeft: 'auto' }}>
                <Button
                  variant="dark"
                  className="me-5"
                  onClick={handleDeleteClick}
                  disabled={isDeleting}
                >
                  {isDeleting ? 'Deleting...' : 'Delete'}
                </Button>
                <input
                  type="checkbox"
                  checked={isChecked}
                  onChange={handleCheckboxToggle}
                  style={{ width: '20px', height: '20px' }}
                />
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
