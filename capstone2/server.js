	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");


// ROUTES
	const userRoutes = require("./routes/userRoutes.js")
	const orderRoutes = require("./routes/orderRoutes.js")
	const productRoutes = require("./routes/productRoutes.js")
	const cartRoutes = require("./routes/productRoutes.js")

	const port = 4001;

	const app = express();

	mongoose.connect("mongodb+srv://admin:admin@batch288alviar.iizabv5.mongodb.net/EcommerceAPI?retryWrites=true&w=majority", {useNewUrlParser: true,
		useUnifiedTopology: true
	});

	let db = mongoose.connection;

	db.on("error", console.error.bind(console, "Error, Can't connect to the database."));

	db.once("open", () => console.log("Connected to the cloud database!"));

	// Middlewares
	app.use(express.json());
	app.use(express.urlencoded({extended:true})); 
	app.use(cors());

	// ROUTING
	app.use("/user", userRoutes);
	app.use("/products", productRoutes);
	app.use("/orders", orderRoutes);
	app.use("/cart", cartRoutes);


	app.listen(port, () => console.log(`The server is running at port ${port}!`));