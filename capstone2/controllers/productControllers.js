const auth = require("../auth.js");

const Product = require("../models/Product.js");
const Cart = require("../models/Cart.js");


module.exports.addProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  if (!userData.isAdmin) {
    return response.send(false);
  }

  Product.findOne({ name: request.body.name })
    .then(result => {
      if (result) {
        return response.send(false);
      } else {
        const newProduct = new Product({
        	brand: request.body.brand,
          name: request.body.name,
          description: request.body.description,
          price: request.body.price
        });

        newProduct
          .save()
          .then(savedProduct => response.json(savedProduct))
          .catch(error => response.send(error));
      }
    });
};

module.exports.getAllProducts = (request,response) => {
		Product.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}

module.exports.getActiveProducts = (request,response) => {
		Product.find({isActive : true})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}

module.exports.getProduct = (request,response) => {

	const productId = request.params.productId

	Product.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
} 

module.exports.getProductIdsOrder = (request,response) => {
  const productIds = request.body.productIds;

  Product.find({ _id: { $in: productIds } })
    .then((results) => response.send(results))
    .catch((error) => response.send(error));
};



module.exports.getBrand = (request, response) => {
  const brand = request.params.brand;

  Product.find({ brand: brand })
    .then((result) => response.send(result))
    .catch((error) => response.send(error));
};

module.exports.updateProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;
  const updatedProductName = request.body.name;

  Product.findOne({ name: updatedProductName, _id: { $ne: productId } })
    .then((existingProduct) => {
      if (existingProduct) {
        return response.send(false);
      }

      let updatedProduct = {
        name: updatedProductName,
        description: request.body.description,
        price: request.body.price,
      };

      if (userData.isAdmin) {
        Product.findByIdAndUpdate(productId, updatedProduct)
          .then((result) => response.send(true))
          .catch((error) => response.send(error));
      } else {
        return response.send(false);
      }
    })
    .catch((error) => response.send(error));
};

module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId

	if(userData.isAdmin) {

		Product.findById(productId)
		.then(result => {
			if(!result.isActive) {
				return response.send(false)
			}else{
				let archivedProduct = {
					isActive: false,
				}

				Product.findByIdAndUpdate(productId, archivedProduct)
				.then(result => {
					Cart.updateMany(
						{ "products.productId": productId },
						{ $pull: { products: { productId: productId } } }
						)
					.then(result =>
						response.send(true)
						)
					.catch(error => response.send(error))
				})
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}else{
		return response.send(false)
	}
}


module.exports.activateProduct = (request,response) => {

	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId

	if(userData.isAdmin){
		Product.findById(productId)
		.then(result => {

			if(result.isActive){

				return response.send(false)

			}else{

				let activatedProduct = {
					isActive: true
				}
				
				Product.findByIdAndUpdate(productId, activatedProduct)
				.then(result => response.send(true))
				.catch(error => response.send(error))
			}
		})
	}else{
		return response.send(false)
	}
}


