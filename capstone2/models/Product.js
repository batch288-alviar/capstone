const mongoose = require("mongoose")


const productSchema = new mongoose.Schema({

	brand:{
		type: String,
		required: true
	},

	name:{
		type: String,
		required: true
	},

	description:{
		type: String,
		required: true
	},

	price:{
		type: Number,
		required: true
	},

	isActive:{
		type: Boolean,
		default: true
	},

	createdOn:{
		type: Date,
	default: new Date()
	}
})

const Product = mongoose.model("Product", productSchema);

module.exports = Product;