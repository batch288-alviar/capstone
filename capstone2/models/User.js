const mongoose = require("mongoose")


const userSchema = new mongoose.Schema({

	firstName:{
		type: String,
		required: [true, "First Name is required!"]
	},

	lastName:{
		type: String,
		required: [true, "Last Name is required!"]
	},

	email:{
		type: String,
		required: [true, "Email is required!"]
	},

	address:{
		type: String,
		required: [true, "Address is required!"]
	},

	password:{
		type: String,
		required: [true, "Password is required!"]
	},

	isAdmin:{
		type: Boolean,
		default: false
	},
})

// Model

const Users = mongoose.model("Users", userSchema);

module.exports = Users;