const auth = require("../auth.js");
const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers.js");

// Routes

// Create product
router.post("/addProduct", auth.verify, productControllers.addProduct);

// Get all products
router.get("/", productControllers.getAllProducts);

// Get active products
router.get("/activeProducts", productControllers.getActiveProducts);

router.post("/orderProducts", productControllers.getProductIdsOrder)

// Get specific products based on brand
router.get("/brand/:brand", productControllers.getBrand);

// Get specific product
router.get("/:productId", productControllers.getProduct);

// Update product
router.patch("/:productId", auth.verify, productControllers.updateProduct);

// Archive product
router.patch("/:productId/archive", auth.verify, productControllers.archiveProduct);

// Activate product
router.patch("/:productId/activate", auth.verify, productControllers.activateProduct);

module.exports = router;