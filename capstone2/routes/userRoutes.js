const auth = require("../auth.js")
const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers.js");
const cartControllers = require("../controllers/cartControllers.js")
const orderControllers = require("../controllers/orderControllers.js")

// Routes

// route for registration
router.post("/register", userControllers.registerUser);

// login user
router.post("/login", userControllers.loginUser);

// route for user details for orders
router.post("/userDataOrder", userControllers.retrieveUserDataforOrder);

// route for user details
router.get("/details", auth.verify, userControllers.userDetails);

// route for user details
router.get("/userData", auth.verify, userControllers.retrieveUserData);

// route to make user admin
router.patch("/admin", auth.verify, userControllers.userAdmin);

// route for user cart
router.patch("/:productId/cart", auth.verify, cartControllers.addToCart);

router.delete("/:productId/deleteCart", auth.verify, cartControllers.deleteProductFromCart);

// route for authenticated users
router.get("/:userId/orders", auth.verify, userControllers.userOrders);

// route to get user cart
router.get("/:userId/viewCart", auth.verify, cartControllers.userCart);

// route for order
router.post("/:userId/createOrder", auth.verify, orderControllers.createOrder);

module.exports = router;